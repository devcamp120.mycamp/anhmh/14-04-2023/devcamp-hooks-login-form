import 'bootstrap/dist/css/bootstrap.min.css'
import LogInForm from './components/LogInForm'
import SignUpForm from './components/SignUpForm'
import { BrowserRouter as Router, Link, Routes, Route } from 'react-router-dom'
import { Container, Row, Col, Button } from 'reactstrap'
function App() {
  const SignUpButton = () => {
    console.log("Ấn nút Sign-up!");
  }
  const LogInButton = () => {
    console.log("Ấn nút Log-in!");
  }
  return (
    <Router>
      <div className='App'>
        <div className='auth-wrapper'>
          <div className='auth-inner'>
            <Container>
              <nav className="navbar navbar-expand-lg navbar-light fixed top">
                <ul className="navbar-nav ml-auto">
                  <Button className="sign-up-button"to={'/sign-up'} onChange= {SignUpButton}>Sign Up</Button>
                  <Button className="log-in-button" to={'/log-in'} onChange= {LogInButton}>Log In</Button>
                </ul>
              </nav>
              <Routes>
                <Route exact path="/" element={<LogInForm />} />
                <Route exact path="/log-in" element={<LogInForm />} />
                <Route exact path="/sign-up" element={<SignUpForm />} />
              </Routes>
            </Container>

          </div>
        </div>
      </div>
    </Router>


  );
}

export default App;

/* 
<Link className="nav-link" to={'/log-in'}>Login</Link>
<Link className="nav-link" to={'/sign-up'}>Sign up</Link>
*/