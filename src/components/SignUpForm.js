import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Form, Row, Col, FormGroup, Input, Button, Container } from 'reactstrap'

function SignUpForm() {
    return (
        <div>
            <Container className="rounded p-4">
                <Row>
                    <Col sm={6} className="text-center">
                        <h4>Sign Up for Free!</h4>
                    </Col>
                </Row>
                <Form>
                    <Row>
                        <Col md={3} className="text-center">
                            <FormGroup>
                                <Input type="firstname" id="exampleFirstName" className="form-control" placeholder="First Name *" />
                            </FormGroup>
                        </Col>
                        <Col md={3} className="text-center">
                            <FormGroup>
                                <Input type="lastname" id="exampleLastName" className="form-control" placeholder="Last Name *" />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6} className="text-center">
                            <FormGroup>
                                <Input type="email" id="exampleEmail" className="form-control" placeholder="Email Address" />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6} className="text-center">
                            <FormGroup>
                                <Input type="Password" id="examplePassword" className="form-control" placeholder="Set A Password *" />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6} className="text-center">
                            <Button className="get-started-button">GET STARTED</Button>
                        </Col>
                    </Row>
                </Form>
            </Container>
        </div>
    )
}

export default SignUpForm
