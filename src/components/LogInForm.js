import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Form, Row, Col, FormGroup, Input, Button, Container } from 'reactstrap'

function LogInForm() {
  return (
    <div>
      <Container className="rounded p-4">
        <Row>
          <Col sm={6} className="text-center">
            <h4>Welcome back!</h4>
          </Col>
        </Row>
        <Form>
          <Row>
            <Col md={6} className="text-center">
              <FormGroup>
                <Input type="email" id="exampleEmail" className="form-control" placeholder="Email address" />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={6} className="text-center">
              <FormGroup>
                <Input type="Password" id="examplePassword" className="form-control" placeholder="Password" />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={6} className="text-center"> 
                <Button className="login-button">LOG IN</Button>         
            </Col>
          </Row>
        </Form>
      </Container>

    </div>
  )
}

export default LogInForm





